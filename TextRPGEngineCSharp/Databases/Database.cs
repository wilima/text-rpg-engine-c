﻿using System;
using System.Collections.Generic;

namespace TextRPGEngineCSharp
{
	public class Database<T>
	{
		Dictionary<int, T> database = new Dictionary<int, T>();

		public Database ()
		{
		}

		public void add(int
			id, T value) {
			database.Add (id, value);
		}

		public T getById(int id) {

			return database [id];
		}

		public Dictionary<int, T> getDatabase {
			get {
				return this.database;
			}
		}

		public void delete() {
			database.Clear ();
		}


	}
}

