﻿using System;

namespace TextRPGEngineCSharp
{
	public class GameItems : Database<Item>
	{
		public Item getItemWithName(string name) {
			foreach (Item item in this.getDatabase.Values) {
				if (item.Name.Replace (" ", "").Equals (name.Replace (" ", ""), StringComparison.InvariantCultureIgnoreCase)) {
					return item;
				}
			}
			throw new ItemHasNotFound ();
		}
	}
}

