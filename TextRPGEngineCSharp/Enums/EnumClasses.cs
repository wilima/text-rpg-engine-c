﻿using System;
using System.Collections.Generic;

namespace TextRPGEngineCSharp
{
	public class EnumClasses
	{
		public static readonly EnumClasses HUMAN = new EnumClasses("HUMAN");

		public static IEnumerable<EnumClasses> Values
		{
			get
			{
				yield return HUMAN;
			}
		}

		private readonly string name;

		EnumClasses (string name)
		{
			this.name = name;
		}


		public string Name {
			get {
				return this.name;
			}
		}

		public override string ToString()
		{
			return this.name;
		}

	}
}

