﻿using System;
using System.Collections.Generic;

namespace TextRPGEngineCSharp
{
	public class EnumCommands
	{
		public static readonly EnumCommands GO = new EnumCommands("Go", "Use for moving.", "Go \"direction\"");
		public static readonly EnumCommands TAKE = new EnumCommands("Take", "Use for taking items.", "Take \"name of item\"");
		public static readonly EnumCommands USE = new EnumCommands("Use", "Use for using items.", "Use \"name of item\"");
		public static readonly EnumCommands EXIT = new EnumCommands("Exit", "Use for exit game.", "");
		public static readonly EnumCommands HELP = new EnumCommands("Help", "Use for print this.", "");
		public static readonly EnumCommands INVENTORY = new EnumCommands("Inventory", "Use for print player's inventory", "");
		public static readonly EnumCommands CHARACTER = new EnumCommands("Character", "Use for print character info", "");

		public static IEnumerable<EnumCommands> Values
		{
			get
			{
				yield return GO;
				yield return TAKE;
				yield return USE;
				yield return EXIT;
				yield return HELP;
				yield return INVENTORY;
				yield return CHARACTER;
			}
		}

		private readonly string name;
		private readonly string description;
		private readonly string syntax;

		EnumCommands (string name, string description, string syntax)
		{
			this.name = name;
			this.description = description;
			this.syntax = syntax;
		}
		

		public string Name {
			get {
				return this.name;
			}
		}

		public string Description {
			get {
				return this.description;
			}
		}

		public string Syntax {
			get {
				return this.syntax;
			}
		}

		public override string ToString()
		{
			return this.name;
		}

	}
}

