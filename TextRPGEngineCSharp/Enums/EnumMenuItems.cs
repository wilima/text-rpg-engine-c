﻿using System;
using System.Collections.Generic;

namespace TextRPGEngineCSharp
{
	public class EnumMenuItems
	{
		public static readonly EnumMenuItems NEWGAME = new EnumMenuItems("New Game");
		public static readonly EnumMenuItems HELP = new EnumMenuItems("Help");
		public static readonly EnumMenuItems EXIT = new EnumMenuItems("Exit");

		public static IEnumerable<EnumMenuItems> Values
		{
			get
			{
				yield return NEWGAME;
				yield return HELP;
				yield return EXIT;
			}
		}

		private readonly string name;

		EnumMenuItems (string name)
		{
			this.name = name;
		}


		public string Name {
			get {
				return this.name;
			}
		}

		public override string ToString()
		{
			return this.name;
		}

	}
}

