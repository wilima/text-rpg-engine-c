﻿using System;
using System.Collections.Generic;

namespace TextRPGEngineCSharp
{
	public class EnumConditions
	{
		public static readonly EnumConditions HASITEM = new EnumConditions("Hasitem", "Require item!", "You do not have required item!");

		public static IEnumerable<EnumConditions> Values
		{
			get
			{
				yield return HASITEM;
			}
		}

		private readonly string internalName;
		private readonly string name;
		private readonly string notification;

		EnumConditions (string internalName, string name, string notification)
		{
			this.internalName = internalName;
			this.name = name;
			this.notification = notification;
		}


		public string InternalName {
			get {
				return this.internalName;
			}
		}

		public string Name {
			get {
				return this.name;
			}
		}

		public string Notification {
			get {
				return this.notification;
			}
		}

		public override string ToString()
		{
			return this.name;
		}

	}
}

