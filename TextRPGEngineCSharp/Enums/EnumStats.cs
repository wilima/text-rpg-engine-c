﻿using System;
using System.Collections.Generic;

namespace TextRPGEngineCSharp
{
	public class EnumStats
	{
		public static readonly EnumStats HP = new EnumStats("Healths");
		public static readonly EnumStats MAXHP = new EnumStats("Healths Maximum");
		public static readonly EnumStats MP = new EnumStats("Mana");
		public static readonly EnumStats MAXMP = new EnumStats("Mana Maximum");
		public static readonly EnumStats ATTACK = new EnumStats("Attack");
		public static readonly EnumStats DEFENCE = new EnumStats("Defence");

		public static IEnumerable<EnumStats> Values
		{
			get
			{
				yield return HP;
				yield return MAXHP;
				yield return MP;
				yield return MAXMP;
				yield return ATTACK;
				yield return DEFENCE;
			}
		}

		private readonly string name;

		EnumStats (string name)
		{
			this.name = name;
		}
		

		public string Name {
			get {
				return this.name;
			}
		}

		public override string ToString()
		{
			return this.name;
		}

	}
}

