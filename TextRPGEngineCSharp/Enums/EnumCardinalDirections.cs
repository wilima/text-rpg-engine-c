﻿using System;
using System.Collections.Generic;

namespace TextRPGEngineCSharp
{
	public class EnumCardinalDirections
	{
		public static readonly EnumCardinalDirections NORTH = new EnumCardinalDirections("North", "north");
		public static readonly EnumCardinalDirections SOUTH = new EnumCardinalDirections("South", "south");
		public static readonly EnumCardinalDirections EAST = new EnumCardinalDirections("East", "east");
		public static readonly EnumCardinalDirections WEST = new EnumCardinalDirections("West", "west");

		public static IEnumerable<EnumCardinalDirections> Values
		{
			get
			{
				yield return NORTH;
				yield return SOUTH;
				yield return EAST;
				yield return WEST;
			}
		}

		private readonly string name;
		private readonly string lowerCaseName;

		EnumCardinalDirections (string name, string lowerCaseName)
		{
			this.name = name;
			this.lowerCaseName = lowerCaseName;
		}

		public string Name {
			get {
				return this.name;
			}
		}

		public string LowerCaseName {
			get {
				return this.lowerCaseName;
			}
		}

		public override string ToString()
		{
			return this.name;
		}
		
	}
}

