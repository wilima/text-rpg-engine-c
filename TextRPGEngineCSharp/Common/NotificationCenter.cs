﻿using System;

namespace TextRPGEngineCSharp
{
	public class NotificationCenter
	{
		string msg = null;

		public NotificationCenter ()
		{
		}

		public string Msg {
			get {
				return this.msg;
			}
			set {
				msg = value;
			}
		}

		public bool hasToSay() {
			return (msg != null);
		}
	}
}

