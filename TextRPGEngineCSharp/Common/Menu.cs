﻿using System;

namespace TextRPGEngineCSharp
{
	public class Menu
	{
		bool menuIsRunning = true;

		public bool MenuIsRunning {
			get {
				return this.menuIsRunning;
			}
			set {
				menuIsRunning = value;
			}
		}

		public bool isMenuRunning() {
			return menuIsRunning;
		}

		public void prompt() {
			Printer.printMenu();
			Printer.printNotificationCenter();
			Console.Write("> ");

			ActionValidator.getActionMenu(Console.ReadLine());
		}
	}
}

