﻿using System;

namespace TextRPGEngineCSharp
{
	public class Global
	{
		//Databases
		public static readonly GameMap MAP = new GameMap();
		public static readonly GameItems ITEMS = new GameItems();

		//Menu
		public static readonly Menu MENU = new Menu();

		//Game
		public static readonly Game GAME = new Game();

		//Player
		public static readonly Player PLAYER = new Player();

		//Notification center
		public static readonly NotificationCenter NOTIFICATIONCENTER = new NotificationCenter();

		//Constants
		public static readonly String GAMEDATAFOLDER = "gamedata";
		public static readonly String ITEMSFILE = "items.json";
		public static readonly String ROOMSFILE = "rooms.json";
	}
}

