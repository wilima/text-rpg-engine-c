﻿using System;
using System.Collections.Generic;

namespace TextRPGEngineCSharp
{
	public class Stats
	{
		Dictionary<string, int> stats = new Dictionary<string, int>();

		public Stats (int hp, int maxHp, int mp, int maxMp, int attack, int defence)
		{
			this.stats.Add(EnumStats.HP.Name, hp);
			this.stats.Add(EnumStats.MAXHP.Name, maxHp);
			this.stats.Add(EnumStats.MP.Name, mp);
			this.stats.Add(EnumStats.MAXMP.Name, maxMp);
			this.stats.Add(EnumStats.ATTACK.Name, attack);
			this.stats.Add(EnumStats.DEFENCE.Name, defence);
		}

		public int getValue(string stat) {
			return this.stats [stat];
		}

		public int getValue(EnumStats stat) {
			return this.stats [stat.Name];
		}

		public void setStat(EnumStats stat, int value) {
			this.stats[stat.Name] = value;
		}
	}
}

