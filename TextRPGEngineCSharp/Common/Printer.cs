﻿using System;
using System.Text;

namespace TextRPGEngineCSharp
{
	public class Printer
	{
		public static void printMenu()
		{
			StringBuilder output = new StringBuilder ();

			output.Append  ("---------------\n");
			output.Append  ("| ~Main Menu~ |\n");
			output.Append  ("---------------\n");

			foreach (EnumMenuItems command in EnumMenuItems.Values) {
				output.Append  (command.Name);
				output.Append  ("\n");
			}

			Console.Write (output.ToString ());
		}

		public static void printHelp() {
			StringBuilder output = new StringBuilder();

			output.Append ("Help for commands avalible in the game.\n");
			output.Append ("------------------------------------\n");

			foreach (EnumCommands command in EnumCommands.Values) {
				output.Append (command.Name);
				output.Append (" - ");
				output.Append (command.Syntax);
				output.Append (" - ");
				output.Append (command.Description);
				output.Append ("\n");
			}
			output.Append ("\n");

			Console.Write (output.ToString ());
		}

		public static void printCharacterInfo() {
			StringBuilder output = new StringBuilder();

			output.Append ("--------------------\n");
			output.Append ("| ~Character Info~ |\n");
			output.Append ("--------------------\n");

			output.Append ("Name: ");
			output.Append (Global.PLAYER.Name);
			output.Append ("\n");
			output.Append ("Class: ");
			output.Append (Global.PLAYER.PlayerClass.Name);
			output.Append ("\n");
			//Print stats
			output.Append (printStats(Global.PLAYER.Stats));
			output.Append ("\n");

			Console.Write (output.ToString ());

		}

		public static string printStats(Stats stats) {
			StringBuilder output = new StringBuilder();

			foreach (EnumStats stat in EnumStats.Values) {
				output.Append (stat.Name);
				output.Append (": ");
				output.Append (stats.getValue(stat));
				output.Append ("\n");
			}

			return output.ToString ();
		}

		public static void printInventory() {
			StringBuilder output = new StringBuilder();

			output.Append ("---------------\n");
			output.Append ("| ~Inventory~ |\n");
			output.Append ("---------------\n");

			if (Global.PLAYER.Inventory.ItemList.Count == 0) {
				output.Append ("Inventory is empty");
			} else {
				foreach (Item item in Global.PLAYER.Inventory.ItemList) {
					output.Append (item.Name);
					output.Append (" - ");
					output.Append (item.Description);
					output.Append ("\n");
				}
			}

			output.Append ("\n");

			Console.Write (output.ToString ());
		}

		public static void printNotificationCenter() {
			if (Global.NOTIFICATIONCENTER.hasToSay()) {
				Console.WriteLine ("NOTIFICATION: " + Global.NOTIFICATIONCENTER.Msg);
				Global.NOTIFICATIONCENTER.Msg = null;
			}
		}

		public static void printRoom(Room room) {
			Console.WriteLine ("----------------------------------------------");
			Console.WriteLine (room.Name);
			Console.WriteLine ("----------------------------------------------");
			Console.WriteLine (room.Description + "\n");

			Console.WriteLine ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			Console.WriteLine ("Possible actions:");
			Console.WriteLine ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

			Console.Write (printRoomDirections(room));
			Console.Write (printAvalibleItems(room));
			Console.WriteLine ();
		}

		private static string printRoomDirections(Room room) {
			StringBuilder output = new StringBuilder();

			foreach (EnumCardinalDirections direction in EnumCardinalDirections.Values) {

				if (room.isConnected(direction)) {
					if (room.isConnectedWithCondition(direction)) {
						if (!room.getConnectedCondition(direction).invoke()) {
							output.Append ("(" + room.getConnectedCondition(direction).Type.Name + ")");
						}
					}
					output.Append ("Go " + direction.LowerCaseName);
					output.Append (": ");
					output.Append (Global.MAP.getById(room.getConnectedId(direction)).Name);
					//Maybe command print
					output.Append ("\n");
				}
			}

			return output.ToString ();
		}

		private static string printAvalibleItems(Room room) {
			StringBuilder output = new StringBuilder();

			foreach (Item item in room.getItems()) {
				if (room.isItemWithCondition(item)) {
					output.Append ("(" + room.getItemCondition(item).Type.Name + ")");
				}
				output.Append ("Take " + item.Name);
				output.Append ("\n");
			}

			return output.ToString ();
		}
	}
}

