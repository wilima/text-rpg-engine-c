﻿using System;

namespace TextRPGEngineCSharp
{
	public class Game
	{
		Boolean gameIsRunning;
		Boolean isLoaded = false;

		public bool GameIsRunning {
			get {
				return this.gameIsRunning;
			}
			set {
				gameIsRunning = value;
			}
		}

		public bool IsLoaded {
			get {
				return this.isLoaded;
			}
			set {
				isLoaded = value;
			}
		}
			
		public Action prompt() {
			Room playerRoom = Global.MAP.getById(Global.PLAYER.PossitionOnMap);

			Printer.printRoom(playerRoom);

			Printer.printNotificationCenter();

			Console.Write("> ");

			return ActionValidator.getAction(playerRoom, Console.ReadLine());
		}

		public void moveGameState(Action action) {
			action.invoke ();
		}
	}
}

