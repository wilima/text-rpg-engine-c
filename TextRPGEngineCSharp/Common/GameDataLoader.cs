﻿using System;
using JSONSharp;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace TextRPGEngineCSharp
{
	public class GameDataLoader
	{
		public static void newGame() {
			if (Global.GAME.IsLoaded) {
				Global.MAP.delete ();
				Global.ITEMS.delete ();
			}

			string appPath = AppDomain.CurrentDomain.BaseDirectory;

			string dataPath = appPath + Global.GAMEDATAFOLDER + Path.DirectorySeparatorChar;

			loadItems (dataPath + Global.ITEMSFILE);
			loadRooms (dataPath + Global.ROOMSFILE);
			Global.GAME.IsLoaded = true;
			createNewPlayer ();
		}

		private static void createNewPlayer() {
			Console.WriteLine ("-----------------------");
			Console.WriteLine ("| Creating new player |");
			Console.WriteLine ("-----------------------");

			Console.Write ("Enter name: ");
			Global.PLAYER.Name = Console.ReadLine();
			Console.WriteLine ();

			Global.PLAYER.PossitionOnMap = 1;
			Global.PLAYER.Stats = new Stats (100, 150, 100, 150, 10, 5);
			Global.PLAYER.PlayerClass = EnumClasses.HUMAN;
		}

		public static void newTestGame() {
			//Test items
			Global.ITEMS.add(1, new Item(1, "Test item 1", "This is test item 1"));
			Global.ITEMS.add(2, new Item(2, "Health Potion", "This is health potiton", ActionGenerator.generateAction("healByItem", 2, 10)));

			//Test Room 1
			Room testRoom1 = new Room(1, "Test Room 1", "This is test room 1");

			testRoom1.addConnected(EnumCardinalDirections.NORTH, 2);
			int[]
			testArray = new int[2];
			testArray[0] = 1;
			testArray[1] = 1;
			testRoom1.addConnected(EnumCardinalDirections.EAST, 3, ConditionGenerator.generateCondition(EnumConditions.HASITEM, testArray));

			Global.MAP.add(1, testRoom1);

			//Test Room 2
			Room testRoom2 = new Room(2, "Test Room 2", "This is test room 2");

			testRoom2.addConnected(EnumCardinalDirections.SOUTH, 1);
			testRoom2.addItem(Global.ITEMS.getById(1), "Take the Test Item 1");
			testRoom2.addItem(Global.ITEMS.getById(2), "Take the Health Potion");

			Global.MAP.add(2, testRoom2);

			//Test Room 3
			Room testRoom3 = new Room(3, "Test Room 3", "This is test room 3");

			testRoom3.addConnected(EnumCardinalDirections.WEST, 1);

			Global.MAP.add(3, testRoom3);
		}

		private static void loadItems(string jsonFile) {
			var itemsFile = System.IO.File.ReadAllText(jsonFile);
			var items = JArray.Parse(itemsFile);

			foreach (JObject itemData in items) {
				int itemId = (int)itemData ["id"];
				string name = (string)itemData ["name"];
				string description = (string)itemData ["description"];
				JToken action = itemData ["action"];

				if (action != null) {
					string actionString = (string)action ["name"];
					JArray actionParams = JArray.Parse (action ["params"].ToString ());

					object[] parameters = new object[actionParams.Count];

					for (int i = 0; i < actionParams.Count; i++) {
						parameters [i] = (int)actionParams [i];
					}

					Global.ITEMS.add(itemId, new Item(itemId, name, description, ActionGenerator.generateAction(actionString, (object)parameters)));
				} else {
					Global.ITEMS.add(itemId, new Item(itemId, name, description));
				}
			}
		}

		private static void loadRooms(string jsonFile) {
			var roomsFile = System.IO.File.ReadAllText(jsonFile);
			var rooms = JArray.Parse(roomsFile);
		
			foreach (JObject roomData in rooms)
			{
				//Console.WriteLine(roomData ["id"]);
				Room room = new Room ((int)roomData ["id"], (string)roomData ["name"], (string)roomData ["text"]);

				var connected = JObject.Parse(roomData ["connected"].ToString());

				foreach (JProperty direction in connected.Children()) {
					string directionString = (string)direction.Name;
					int roomId = (int)direction.Value ["roomId"];
					JToken condition = direction.Value ["condition"];
					EnumCardinalDirections directionEnum = null;

					switch (directionString)
					{
					case "north":
						directionEnum = EnumCardinalDirections.NORTH;
						break;
					case "south":
						directionEnum = EnumCardinalDirections.SOUTH;
						break;
					case "east":
						directionEnum = EnumCardinalDirections.EAST;
						break;
					case "west":
						directionEnum = EnumCardinalDirections.WEST;
						break;
					default:
						break;
					}

					if (condition != null) {
						string conditionString = (string)condition ["name"];
						JArray conditionParams = JArray.Parse (condition ["params"].ToString ());
						EnumConditions conditionEnum = null;

						switch (conditionString)
						{
						case "hasItem":
							conditionEnum = EnumConditions.HASITEM;
							break;
						default:
							break;
						}

						object[] parameters = new object[conditionParams.Count];

						for (int i = 0; i < conditionParams.Count; i++) {
							parameters [i] = (int)conditionParams [i];
						}

						room.addConnected(directionEnum, roomId, ConditionGenerator.generateCondition(conditionEnum, (object)parameters));
					} else {
						room.addConnected(directionEnum, roomId);
					}

				}

				var items = JArray.Parse(roomData ["items"].ToString());

				foreach (JToken item in items.Children()) {
					int itemId = (int)item ["id"];
					string text = (string)item ["text"];
					JToken condition = item ["condition"];

					if (condition != null) {
						string conditionString = (string)condition ["name"];
						JArray conditionParams = JArray.Parse (condition ["params"].ToString ());
						EnumConditions conditionEnum = null;

						switch (conditionString) {
						case "hasItem":
							conditionEnum = EnumConditions.HASITEM;
							break;
						default:
							break;
						}

						object[] parameters = new object[conditionParams.Count];

						for (int i = 0; i < conditionParams.Count; i++) {
							parameters [i] = (int)conditionParams [i];
						}

						room.addItem(Global.ITEMS.getById(itemId), text, ConditionGenerator.generateCondition (conditionEnum, (object)parameters));
					} else {
						room.addItem(Global.ITEMS.getById(itemId), text);
					}
				}

				Global.MAP.add((int)roomData ["id"], room);
			}
		}

	}
}

