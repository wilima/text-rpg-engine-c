﻿using System;

namespace TextRPGEngineCSharp
{
	public class GameLoops
	{
		public static void generalLoop ()
		{
			while (Global.GAME.GameIsRunning) {
				Action action = Global.GAME.prompt ();
				Global.GAME.moveGameState(action);
			}
		}

		public static void menuLoop ()
		{
			while (Global.MENU.MenuIsRunning) {
				Global.MENU.prompt ();
			}
		}
	}
}

