﻿using System;

namespace TextRPGEngineCSharp
{
	public class Player
	{
		int possitionOnMap;
		Inventory inventory = new Inventory();
		Stats stats;
		string name;
		EnumClasses playerClass;

		public Player ()
		{
		}

		public int PossitionOnMap {
			get {
				return this.possitionOnMap;
			}
			set {
				possitionOnMap = value;
			}
		}

		public Stats Stats {
			get {
				return this.stats;
			}
			set {
				stats = value;
			}
		}

		public string Name {
			get {
				return this.name;
			}
			set {
				name = value;
			}
		}

		public EnumClasses PlayerClass {
			get {
				return this.playerClass;
			}
			set {
				playerClass = value;
			}
		}

		public Inventory Inventory {
			get {
				return this.inventory;
			}
		}
	}
}

