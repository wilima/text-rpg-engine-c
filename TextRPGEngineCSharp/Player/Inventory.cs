﻿using System;
using System.Collections.Generic;

namespace TextRPGEngineCSharp
{
	public class Inventory
	{
		List<Item> inventory = new List<Item>();

		public bool hasItem(Item item) {
			return inventory.Contains (item);
		}

		public void addItem(Item item) {
			inventory.Add (item);
		}

		public void removeItem(Item item) {
			inventory.Remove (item);
		}

		public List<Item> ItemList {
			get {
				return this.inventory;
			}
		}
	}
}

