﻿using System;

namespace TextRPGEngineCSharp
{
	public class TakeAction : ActionClass
	{
		public TakeAction (object[] args) : base (args)
		{
		}

		override public void invoke() {
			Room room = (Room) args[0];
			Item item = (Item) args[1];

			room.removeItem (item);
			Global.PLAYER.Inventory.addItem (item);
		}
	}
}

