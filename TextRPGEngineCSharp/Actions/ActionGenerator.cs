﻿using System;

namespace TextRPGEngineCSharp
{
	public class ActionGenerator
	{
		private static Action generateActionSwitch(string name, object[] objects) {
			switch (name) {
			case "move":
				return new MoveAction (objects);
			case "take":
				return new TakeAction (objects);
			case "healByItem":
				return new HealByItemAction (objects);
			case "none":
				return new NoneAction (objects);
			default:
				throw new UnknowActionType ();
			}
		}

		public static Action generateAction(string name, params object[] objects) 
		{
			if (objects.Length != 0) {
				if (objects [0].GetType ().IsArray) {
					object[] args = (object[])objects [0];
					return generateActionSwitch (name, args);
				}
			}
			return generateActionSwitch (name, objects);
		}
	}
}

