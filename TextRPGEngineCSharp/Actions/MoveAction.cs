﻿using System;

namespace TextRPGEngineCSharp
{
	public class MoveAction : ActionClass
	{

		public MoveAction (object[] args) : base (args)
		{
		}

		override public void invoke() {
			int roomId = (int) args[0];

			Global.PLAYER.PossitionOnMap = roomId;
		}
	}
}

