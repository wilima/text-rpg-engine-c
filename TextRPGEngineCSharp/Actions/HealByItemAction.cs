﻿using System;

namespace TextRPGEngineCSharp
{
	public class HealByItemAction : ActionClass
	{
		public HealByItemAction (object[] args) : base (args)
		{
		}

		override public void invoke() {
			int itemId = (int) args[0];
			int howMuch = (int) args[1];

			Item item = Global.ITEMS.getById(itemId);

			if (Global.PLAYER.Stats.getValue(EnumStats.HP) ==
				Global.PLAYER.Stats.getValue(EnumStats.MAXHP)) {
				Global.NOTIFICATIONCENTER.Msg = "Your healts are full!";
			} else {
				Global.PLAYER.Inventory.removeItem(item);

				int hp = Global.PLAYER.Stats.getValue(EnumStats.HP) + howMuch;
				if (hp > Global.PLAYER.Stats.getValue(EnumStats.MAXHP)) {
					hp = Global.PLAYER.Stats.getValue(EnumStats.MAXHP);
				}

				Global.PLAYER.Stats.setStat(EnumStats.HP, hp);
			}
		}
	}
}

