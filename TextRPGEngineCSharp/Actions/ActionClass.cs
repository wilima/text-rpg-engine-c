﻿using System;

namespace TextRPGEngineCSharp
{
	public abstract class ActionClass : Action
	{
		public object[] args;

		public ActionClass (params object[] args)
		{
			this.args = args;
		}

		public abstract void invoke ();
	}
}

