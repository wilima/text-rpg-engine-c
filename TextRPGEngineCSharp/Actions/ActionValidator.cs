﻿using System;
using System.Text;

namespace TextRPGEngineCSharp
{
	public class ActionValidator
	{
		public static void getActionMenu(string command) {
			Console.Clear ();
			if (string.Equals(command, EnumMenuItems.NEWGAME.Name, StringComparison.OrdinalIgnoreCase)) {
				//GameDataLoader.newTestGame();
				try {
					GameDataLoader.newGame();
				} catch (ErrorDuringReadingGameDataFile e) {
					throw new UnableToStartNewGame();
				}
				Global.GAME.GameIsRunning = true;
				GameLoops.generalLoop();
			} else if (string.Equals(command, EnumMenuItems.HELP.Name, StringComparison.OrdinalIgnoreCase)) {
				Printer.printHelp();
			} else if (string.Equals(command, EnumMenuItems.EXIT.Name, StringComparison.OrdinalIgnoreCase)) {
				Global.MENU.MenuIsRunning = false;
			} else {
				Global.NOTIFICATIONCENTER.Msg = "Unknown command";
			}
		}

		public static Action getAction(Room room, string command) {
			string[] parsed = command.Split(' ');

			Console.Clear ();

			if (string.Equals(parsed[0], EnumCommands.GO.Name, StringComparison.OrdinalIgnoreCase)) {
				return validateMovement(room, parsed);
			}

			if (string.Equals(parsed[0], EnumCommands.TAKE.Name, StringComparison.OrdinalIgnoreCase)) {
				return validateObtainingItems(room, parsed);
			}

			if (string.Equals(parsed[0], EnumCommands.USE.Name, StringComparison.OrdinalIgnoreCase)) {
				return validateUsingItems(parsed);
			}

			if (string.Equals(parsed[0], EnumCommands.HELP.Name, StringComparison.OrdinalIgnoreCase)) {
				Printer.printHelp();
				return ActionGenerator.generateAction("none");
			}

			if (string.Equals(parsed[0], EnumCommands.CHARACTER.Name, StringComparison.OrdinalIgnoreCase)) {
				Printer.printCharacterInfo();
				return ActionGenerator.generateAction("none");
			}

			if (string.Equals(parsed[0], EnumCommands.INVENTORY.Name, StringComparison.OrdinalIgnoreCase)) {
				Printer.printInventory();
				return ActionGenerator.generateAction("none");
			}

			if (string.Equals(parsed[0], EnumCommands.EXIT.Name, StringComparison.OrdinalIgnoreCase)) {
				Global.GAME.GameIsRunning = false;
				return ActionGenerator.generateAction("none");
			}

			Global.NOTIFICATIONCENTER.Msg = "Invalid command! - Type \"help\" for help.";
			return ActionGenerator.generateAction("none");

		}

		static Action validateMovement(Room room, string[] parsed) {
			Action action;

			try {
				EnumCardinalDirections direction = null;

				foreach (EnumCardinalDirections dir in EnumCardinalDirections.Values) {
					if (dir.LowerCaseName == parsed[1]) {
						direction = dir;
					}
				}

				if (room.isConnected(direction)) {
					if (room.isConnectedWithCondition(direction)) {
						if (!room.getConnectedCondition(direction).invoke()) {
							Global.NOTIFICATIONCENTER.Msg = room.getConnectedCondition(direction).Type.Notification;
							action = ActionGenerator.generateAction("none");
						} else {
							action =  ActionGenerator.generateAction("move", room.getConnectedId(direction));
						}
					} else {
						action =  ActionGenerator.generateAction("move", room.getConnectedId(direction));
					}

					return action;
				} else {
					throw new DirectionIsNotValid ();
				}
			} catch (Exception e) {
				Global.NOTIFICATIONCENTER.Msg = "There is no path like this!";
				action =  ActionGenerator.generateAction("none");
				return action;
			}
		}

		static Action validateUsingItems(string[] parsed) {
			Action action;

			StringBuilder itemNameStringBuilder = new StringBuilder();

			for (int i = 1; i < parsed.Length; i++) {
				itemNameStringBuilder.Append (parsed[i]);
			}

			string itemName = itemNameStringBuilder.ToString();

			try {
				Item item = Global.ITEMS.getItemWithName(itemName);
				if (item.Usable) {
					if (Global.PLAYER.Inventory.hasItem(item)) {
						action = item.UseAction;
					} else {
						throw new ItemHasNotFound();
					}
				} else {
					throw new ItemIsNotUsable();
				}
				return action;

			} catch (ItemHasNotFound e) {
				Global.NOTIFICATIONCENTER.Msg = "You do not have this item!";
				action = ActionGenerator.generateAction("move", Global.PLAYER.PossitionOnMap);
				return action;
			} catch (ItemIsNotUsable e) {
				Global.NOTIFICATIONCENTER.Msg = "Item is not usable!";
				action = ActionGenerator.generateAction("move", Global.PLAYER.PossitionOnMap);
				return action;
			}

		}

		static Action validateObtainingItems(Room room, string[] parsed) {
			Action action;

			StringBuilder itemNameStringBuilder = new StringBuilder();

			for (int i = 1; i < parsed.Length; i++) {
				itemNameStringBuilder.Append (parsed[i]);
			}

			string itemName = itemNameStringBuilder.ToString();

			try {
				Item item = Global.ITEMS.getItemWithName(itemName);
				if (room.hasItem(item)) {
					if (room.hasItemWithCondtion(item)) {
						if (!room.getItemCondition(item).invoke()) {
							Global.NOTIFICATIONCENTER.Msg = room.getItemCondition(item).Type.Notification;
							action = ActionGenerator.generateAction("move", Global.PLAYER.PossitionOnMap);
						} else {
							action = ActionGenerator.generateAction("take", room, item);
						}
					} else {
						action = ActionGenerator.generateAction("take", room, item);
					}
				} else {
					throw new ItemHasNotFound();
				}
				return action;

			} catch (ItemHasNotFound e) {
				Global.NOTIFICATIONCENTER.Msg = "There is no item like this!";
				action = ActionGenerator.generateAction("move", Global.PLAYER.PossitionOnMap);
				return action;
			}

		}
	}
}

