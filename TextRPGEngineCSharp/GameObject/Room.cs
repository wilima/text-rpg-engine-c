﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TextRPGEngineCSharp
{
	public class Room : GameObject
	{
		Dictionary<EnumCardinalDirections, int> connected = new Dictionary<EnumCardinalDirections, int>();
		Dictionary<EnumCardinalDirections, Condition> connectedConditions = new Dictionary<EnumCardinalDirections, Condition>();

		Dictionary<Item, string> items = new Dictionary<Item, string>();
		Dictionary<Item, Condition> itemsConditions = new Dictionary<Item, Condition>();

		public Room (int id, string name, string description) : base (id, name, description)
		{
		}

		public void addConnected(EnumCardinalDirections direction, int roomId) {
			connected.Add (direction, roomId);
		}

		public void addConnected(EnumCardinalDirections direction, int roomId, Condition condition) {
			connected.Add (direction, roomId);
			connectedConditions.Add (direction, condition);
		}

		public bool isConnected(EnumCardinalDirections direction) {
			return connected.ContainsKey (direction);
		}

		public bool isConnectedWithCondition(EnumCardinalDirections direction) {
			return connectedConditions.ContainsKey (direction);
		}

		public Condition getConnectedCondition(EnumCardinalDirections direction) {
			return connectedConditions [direction];
		}

		public int getConnectedId(EnumCardinalDirections direction) {
			return connected [direction];
		}

		public void addItem(Item item, string msg) {
			items.Add (item, msg);
		}

		public void addItem(Item item, string msg, Condition condition) {
			items.Add (item, msg);
			itemsConditions.Add (item, condition);
		}

		public Item[] getItems() {
			return items.Keys.ToArray();
		}

		public bool hasItem(Item item) {
			return items.ContainsKey (item);
		}

		public bool hasItemWithCondtion(Item item) {
			return itemsConditions.ContainsKey (item);
		}

		public Condition getItemCondition(Item item) {
			return itemsConditions [item];
		}

		public bool isItemWithCondition(Item item) {
			return itemsConditions.ContainsKey (item);
		}

		public void removeItem(Item item) {
			items.Remove (item);
			if (isItemWithCondition (item)) {
				itemsConditions.Remove (item);
			}
		}

	}
}

