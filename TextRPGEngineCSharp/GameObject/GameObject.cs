﻿using System;

namespace TextRPGEngineCSharp
{
	public class GameObject
	{
		int id;
		string name;
		string description;

		public GameObject (int id, string name, string description)
		{
			this.id = id;
			this.name = name;
			this.description = description;
		}

		public int Id {
			get {
				return this.id;
			}
			set {
				id = value;
			}
		}

		public string Name {
			get {
				return this.name;
			}
			set {
				name = value;
			}
		}

		public string Description {
			get {
				return this.description;
			}
			set {
				description = value;
			}
		}
			
	}
}

