﻿using System;

namespace TextRPGEngineCSharp
{
	public class Item : GameObject
	{
		bool usable;
		Action useAction;

		public Item (int id, string name, string description, Action useAction) : base (id, name, description)
		{
			this.useAction = useAction;
			this.usable = true;
		}

		public Item (int id, string name, string description) : base (id, name, description)
		{
			this.usable = false;
		}

		public bool Usable {
			get {
				return this.usable;
			}
		}

		public Action UseAction {
			get {
				return this.useAction;
			}
		}

	}
}

