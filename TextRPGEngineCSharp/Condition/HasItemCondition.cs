﻿using System;

namespace TextRPGEngineCSharp
{
	public class HasItemCondition : ConditionClass
	{
		public HasItemCondition (EnumConditions type,  object[] args) : base (type, args)
		{
		}

		override public bool invoke() {
			int itemId = (int) args[0];

			//For future use
			int itemQuantity = (int) args[1];

			Item item = Global.ITEMS.getById(itemId);

			return (Global.PLAYER.Inventory.hasItem(item));
		}
	}
}

