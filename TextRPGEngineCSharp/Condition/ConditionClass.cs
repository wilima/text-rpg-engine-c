﻿using System;

namespace TextRPGEngineCSharp
{
	public abstract class ConditionClass : Condition
	{
		public Object[] args;
		public EnumConditions type;

		public ConditionClass (EnumConditions type, object[] args)
		{
			this.args = args;
			this.type = type;
		}

		public EnumConditions Type {
			get {
				return this.type;
			}
		}

		public abstract bool invoke ();
	}
}

