﻿using System;

namespace TextRPGEngineCSharp
{
	public interface Condition
	{
		bool invoke ();
		EnumConditions Type 
		{ 
			get;
		}
	}
}

