﻿using System;

namespace TextRPGEngineCSharp
{
	public static class ConditionGenerator
	{
		private static Condition generateConditionSwitch(EnumConditions type, object[] objects) {
			switch (type.InternalName) {

			case "Hasitem":

				return new HasItemCondition (EnumConditions.HASITEM, objects);

			default:
				return null;
			}
		}
		public static Condition generateCondition(EnumConditions type, params object[] objects) {
			if (objects.Length != 0) {
				if (objects [0].GetType ().IsArray) {
					object[] args = (object[])objects [0];
					return generateConditionSwitch (type, args);
				}
			}
			return generateConditionSwitch (type, objects);
			
		}
	}
}

